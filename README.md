# Reksoft testcase

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Features

- **Django 4.1** Tested with Python 3.10
- **SQLite 3.39** 
- **Black 22.12**
- **Botstrap 5.3** with CDN

## Installation

```bash
tar -xvf test-task-semernyakov && cd ./test-task-semernyakov
python3 --m venv env && source ./env/bin/activate
```

## Usage

Although this is not required! Admin panel works without authorization!
```bash
./manage.py createsuperuser --email="mail@site.com" --username="username"
```

## Linters and Formatterse

```bash
black .
black -t py310 -l 79 --check
black -t py310 -l 79 .
```
&copy; 2023, Ivan Semernyakov [beatuminflow@gmail.com](mailto:beatuminflow@gmail.com)

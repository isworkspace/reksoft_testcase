from django.db import models
from django.db import transaction
from utils.deocorators import query_debugger


class TruckModel(models.Model):
    """Модели машин"""

    truck_model = models.CharField("Наименование модели", max_length=64, unique=True)

    def __str__(self):
        return f"{self.truck_model}"


class TruckCapacity(models.Model):
    """Максимальная грузоподъёмность машин"""

    truck_capacity = models.FloatField("Максимальная грузоподъёмность", unique=True)

    def __str__(self):
        return f"{self.truck_capacity}"

    @query_debugger
    def update_related_truck(self):
        # массовое пересохранение записей в связанной модели
        if self.id:
            t = Truck.related_model.filter(trucks_capacity_name_id=self.id)
            if t:
                try:
                    with transaction.atomic():
                        for i in t:
                            i.save()
                except Exception as e:
                    print(f"Errors: {e}")

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        self.update_related_truck()


class TruckManager(models.Manager):
    """Менеджер модели Track"""

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("trucks_model_name", "trucks_capacity_name")
        )


class Truck(models.Model):
    """Грузовые Машины"""

    # модель
    trucks_model_name = models.ForeignKey(
        TruckModel,
        null=True,
        verbose_name="Модель машины",
        on_delete=models.DO_NOTHING,
    )
    # бортовой номер
    truck_number = models.CharField("Бортовой номер", max_length=64, unique=True)

    # згрузоподьёмность
    trucks_capacity_name = models.ForeignKey(
        TruckCapacity,
        verbose_name="Максимальная грузоподъёмность",
        on_delete=models.DO_NOTHING,
    )
    # текущий вес
    truck_workload = models.FloatField("Текущий вес, т.", blank=True, null=True)

    # перегрузка
    truck_overload = models.FloatField(
        "Перегрузка, %", blank=True, null=True, editable=False
    )

    related_model = TruckManager()

    @staticmethod
    def percent_overload(x, y):
        """Вывод перегрузки в %"""
        if not x and not y or x <= 0:
            """ZeroDivisionError - Перехват исключения и возврата нулевого значения)"""
            return None
        elif y is None and x > 0:
            """"""
            return None
        elif not x <= y or y <= 0 or x == y:
            """Отсекаем отрицательные значения и всё то, что, не является перегрузкой')"""
            return None
        else:
            return round((y / x) * 100, 2)

    def save(self, *args, **kwargs):
        self.truck_overload = self.percent_overload(
            self.trucks_capacity_name.truck_capacity, self.truck_workload
        )
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.trucks_model_name} №{self.truck_number}"

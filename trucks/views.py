from django.http import Http404
from django.shortcuts import render
from django.views.generic import View

from utils.deocorators import query_debugger
from .models import Truck, TruckModel


class Home(View):
    @query_debugger
    def get(self, request, *args, **kwargs):
        try:

            trucks = Truck.related_model.all()
            models = TruckModel.objects.filter(truck__in=trucks).distinct()

        except TruckModel.DoesNotExist:
            raise Http404("No Truck Model's found.")

        except Truck.DoesNotExist:
            raise Http404("No Truck's found.")

        return render(
            request, "trucks/index.html", {"trucks": trucks, "models": models}
        )

    @query_debugger
    def post(self, request):
        cur_model_id = None

        if request.POST.get("model_id") == "None":

            trucks = Truck.related_model.all()
            models = TruckModel.objects.exclude(truck=None)

        else:
            cur_model_id = request.POST.get("model_id")

            trucks = Truck.related_model.filter(trucks_model_name_id=cur_model_id)
            models = TruckModel.objects.exclude(truck=None)

            request.session["cur_model_id"] = cur_model_id

            if type(cur_model_id) == str:
                cur_model_id = int(cur_model_id)

        return render(
            request,
            "trucks/index.html",
            {"trucks": trucks, "models": models, "cur_model_id": cur_model_id},
        )

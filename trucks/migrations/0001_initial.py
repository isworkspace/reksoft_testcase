# Generated by Django 4.1.5 on 2023-02-01 08:44

import django.db.models.deletion
import django.db.models.manager
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="TruckCapacity",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "truck_capacity",
                    models.FloatField(
                        unique=True,
                        verbose_name="Максимальная грузоподъёмность",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="TruckModel",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "truck_model",
                    models.CharField(
                        max_length=64,
                        unique=True,
                        verbose_name="Наименование модели",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Truck",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "truck_number",
                    models.CharField(
                        max_length=64,
                        unique=True,
                        verbose_name="Бортовой номер",
                    ),
                ),
                (
                    "truck_workload",
                    models.FloatField(
                        blank=True, null=True, verbose_name="Текущий вес, т."
                    ),
                ),
                (
                    "truck_overload",
                    models.FloatField(
                        blank=True,
                        editable=False,
                        null=True,
                        verbose_name="Перегрузка, %",
                    ),
                ),
                (
                    "trucks_capacity_name",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="trucks.truckcapacity",
                        verbose_name="Максимальная грузоподъёмность",
                    ),
                ),
                (
                    "trucks_model_name",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="trucks.truckmodel",
                        verbose_name="Модель машины",
                    ),
                ),
            ],
            managers=[
                ("related_model", django.db.models.manager.Manager()),
            ],
        ),
    ]

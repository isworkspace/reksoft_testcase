from django.contrib import admin

from .models import Truck, TruckCapacity, TruckModel

admin.site.register(Truck)
admin.site.register(TruckModel)
admin.site.register(TruckCapacity)

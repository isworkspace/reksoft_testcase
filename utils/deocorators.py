import functools
import time

from django.db import connection


def query_debugger(func):
    """Измеряет время и кол-во запросов"""

    @functools.wraps(func)
    def inner_func(*args, **kwargs):
        start_queries = len(connection.queries)

        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()

        end_queries = len(connection.queries)

        print(f"Function : {func.__name__}")
        print(f"Number of Queries : {end_queries - start_queries}")
        print(f"Finished in : {(end - start):.3f}s")
        return result

    return inner_func
